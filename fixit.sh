#!/bin/sh

# Reference: http://www.tonymacx86.com/graphics/161256-solving-nvidia-driver-install-problems.html
#
# This script automates the modification of AppleGraphicsDevicePolicy.kext so that the
# monitor does not go to end of sleep at boot sequence when using NVIDA card.
#

id="/usr/bin/id"
awk="/usr/bin/awk"

tmp=`$id | $awk -F" " '{print $1}'`

if [ "$tmp" = 'uid=0(root)' ]; then
   echo "You are root!"
   tool=/usr/libexec/PlistBuddy

   # Real Path
   plist="/System/Library/Extensions/AppleGraphicsControl.kext/Contents/PlugIns/AppleGraphicsDevicePolicy.kext/Contents/Info.plist"

   #Backup plist, just in case
   /bin/cp $plist $plist.backup

   echo "\nModifying $plist \n"

   echo "Current Values"
   echo "=============="
   tmp=`$tool -c "Print :IOKitPersonalities:AppleGraphicsDevicePolicy:ConfigMap:Mac-42FD25EABCABB274" $plist`
   echo "iMac15,1 = $tmp"
   tmp=`$tool -c "Print :IOKitPersonalities:AppleGraphicsDevicePolicy:ConfigMap:Mac-F60DEB81FF30ACF6" $plist`
   echo "MacPro6,1 = $tmp"
                                                                                                                                               
                                                                                                                                               
   echo "\nSetting Values to none"
   tmp=`$tool -c "Set :IOKitPersonalities:AppleGraphicsDevicePolicy:ConfigMap:Mac-42FD25EABCABB274 none" $plist`
   tmp=`$tool -c "Set :IOKitPersonalities:AppleGraphicsDevicePolicy:ConfigMap:Mac-F60DEB81FF30ACF6 none" $plist`


   echo "\nVerfying Values (should be none)"
   echo "=================================="
   tmp=`$tool -c "Print :IOKitPersonalities:AppleGraphicsDevicePolicy:ConfigMap:Mac-42FD25EABCABB274" $plist`
   echo "iMac15,1 = $tmp"
   tmp=`$tool -c "Print :IOKitPersonalities:AppleGraphicsDevicePolicy:ConfigMap:Mac-F60DEB81FF30ACF6" $plist`
   echo "MacPro6,1 = $tmp"

   echo "\nRebuilding Kernel Cache"
   /usr/sbin/kextcache -system-prelinked-kernel
   /usr/sbin/kextcache -system-caches

else
   echo "This script will not work with your userid, type 'sudo fixit.sh'"
fi;
